#include <iostream>
#include "Ran.h"
#include <ctime>
#include <cstdlib>
using namespace std;
#define DOT_RAN 254
#define MAX 100
#define LEN 1
#define XUONG 2
#define TRAI 3
#define PHAI 4
#define TUONG_TREN 2
#define TUONG_DUOI 20
#define TUONG_TRAI 4
#define TUONG_PHAI 72
struct ToaDo
{
    int x;
    int y;
};
ToaDo Ran[MAX];
int soDot = 3;
int Diem = 0;
void KhoiTaoRan()
{
    Ran[0].x = TUONG_TRAI + 3;
    Ran[1].x = TUONG_TRAI + 2;
    Ran[2].x = TUONG_TRAI + 1;
    Ran[0].y = Ran[1].y = Ran[2].y = TUONG_TREN + 1;
}
void HienThiRan(ToaDo dotCuoi)
{
    gotoXY(Ran[0].x, Ran[0].y);
    cout << "@";
    for (int i = 1; i < soDot; i++)
    {
        gotoXY(Ran[i].x, Ran[i].y);
        cout << (char)DOT_RAN;
    }
    gotoXY(dotCuoi.x, dotCuoi.y);
    cout << " ";

}
ToaDo DiChuyen(int huong)
{
    ToaDo dotCuoi = Ran[soDot - 1];
    for (int i = soDot; i >= 1; i--)
    {
        Ran[i] = Ran[i - 1];
    }
    switch (huong)
    {
    case LEN:
        Ran[0].y--;
        break;
    case XUONG:
        Ran[0].y++;
        break;
    case TRAI:
        Ran[0].x--;
        break;
    case PHAI:
        Ran[0].x++;
        break;
    }
    return dotCuoi;
}
void BanPhim(int& huong)
{
    int key = inputKey();
    if (key == 'w' || key == 'W' || key == KEY_UP)
    {
        huong = LEN;
    }
    else if (key == 's' || key == 'S' || key == KEY_DOWN)
    {
        huong = XUONG;
    }
    else if (key == 'd' || key == 'D' || key == KEY_RIGHT)
    {
        huong = PHAI;
    }
    else if (key == 'a' || key == 'A' || key == KEY_LEFT)
    {
        huong = TRAI;
    }

}
void VeKhung()
{
    for (int i = TUONG_TRAI; i <= TUONG_PHAI; i++)
    {
        gotoXY(i, TUONG_TREN);
        cout << (char)220;
    }
    for (int i = TUONG_TREN + 1; i <= TUONG_DUOI; i++)
    {
        gotoXY(TUONG_TRAI, i);
        cout << (char)221;
    }
    for (int i = TUONG_TRAI; i <= TUONG_PHAI; i++)
    {
        gotoXY(i, TUONG_DUOI);
        cout << (char)223;
    }
    for (int i = TUONG_TREN + 1; i <= TUONG_DUOI - 1; i++)
    {
        gotoXY(TUONG_PHAI, i);
        cout << (char)222;
    }

}
bool KTThua()
{
    if (Ran[0].y == TUONG_TREN)
        return true;
    if (Ran[0].y == TUONG_DUOI)
        return true;
    if (Ran[0].x == TUONG_TRAI)
        return true;
    if (Ran[0].x == TUONG_PHAI)
        return true;
    for (int i = 1; i < soDot; i++)
    {
        if (Ran[0].x == Ran[i].x && Ran[0].y == Ran[i].y)
            return true;
    }
}
void XuLiThua()
{
    system("cls");
    cout << "Game over, hic";
    Sleep(5000);

}
ToaDo HienThiMoi()
{
    srand(time(NULL));
    int x = rand() % (TUONG_PHAI - TUONG_TRAI + 1) + TUONG_TRAI;
    int y = rand() % (TUONG_TREN - TUONG_DUOI + 1) + TUONG_TREN;
    gotoXY(x, y);
    cout << "*";
    return ToaDo{ x,y };
}
bool KTAnMoi(ToaDo Moi)
{
    if (Ran[0].x == Moi.x && Ran[0].y == Moi.y)
    {
        return true;
    }
    else
    {
        return false;
    }
}
void ThemDot()
{
    Ran[soDot] = Ran[soDot - 1];
    soDot++;
}
void main()
{
    int huong = PHAI;
    int Dokho = 0;
    cout << "Moi nhap do kho tu 100->500 tu kho den de:";
    cin >> Dokho;
    system("cls");
    KhoiTaoRan();
    VeKhung();
    ToaDo Moi = HienThiMoi();
    while (1)
    {
        ToaDo dotCuoi = DiChuyen(huong);
        BanPhim(huong);
        HienThiRan(dotCuoi);
        if (KTAnMoi(Moi) == true)
        {
            Moi = HienThiMoi();
            ThemDot();
            Diem++;
            gotoXY(TUONG_PHAI + 3, TUONG_TREN);
            cout << "DIEM: " << Diem;
        }
        if (KTThua() == true)
            break;
        Sleep(Dokho);

    }
    XuLiThua();

}
